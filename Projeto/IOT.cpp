#include <Arduino.h>
#include "Projeto.h"

long int TempoTotal = 0;
extern Ultrasonic ultrasonic;

WiFiClient client;
Adafruit_MQTT_Client mqtt(&client, "io.adafruit.com", 1883 , IO_USERNAME, IO_KEY);

Adafruit_MQTT_Subscribe _LampadaS = Adafruit_MQTT_Subscribe(&mqtt, "carranca/f/Lampada", MQTT_QOS_1);
Adafruit_MQTT_Subscribe _Ventilador = Adafruit_MQTT_Subscribe(&mqtt, "carranca/f/Ventilador", MQTT_QOS_1);
Adafruit_MQTT_Subscribe _ControleS = Adafruit_MQTT_Subscribe(&mqtt, "carranca/f/Manual", MQTT_QOS_1);

Adafruit_MQTT_Publish _TempoLampada = Adafruit_MQTT_Publish(&mqtt, "carranca/f/TempoEnergia", MQTT_QOS_1);
Adafruit_MQTT_Publish _ControleP = Adafruit_MQTT_Publish(&mqtt, "carranca/f/Manual", MQTT_QOS_1);
Adafruit_MQTT_Publish _LampadaP = Adafruit_MQTT_Publish(&mqtt, "carranca/f/Lampada", MQTT_QOS_1);
Adafruit_MQTT_Publish _Temperatura = Adafruit_MQTT_Publish(&mqtt, "carranca/f/Temperatura", MQTT_QOS_1);
Adafruit_MQTT_Publish _VentiladorP = Adafruit_MQTT_Publish(&mqtt, "carranca/f/Ventilador", MQTT_QOS_1);

void initMQTT() {
  _LampadaS.setCallback(Lampada_callback);
  mqtt.subscribe(&_LampadaS);

  _Ventilador.setCallback(Ventilador_callback);
  mqtt.subscribe(&_Ventilador);

  _ControleS.setCallback(Controle_callback);
  mqtt.subscribe(&_ControleS);
}

void Temperatura_publish(void) {
  int valor = (int) temperatura(10, DELAY_);

  if (!_Temperatura.publish(valor))  //envia e testa ao mesmo tempo
    Serial.print(F("\nFalha ao enviar Temperatura!"));
  else
    Serial.println(F("\nTemperatura enviado!"));

  Serial.println(valor);
}

void TempoLampada_publish(void) {
  int valor = (int) TempoTotal/60000;

  if (!_TempoLampada.publish(valor))  //envia e testa ao mesmo tempo
    Serial.print(F("\nFalha ao enviar Temperatura!"));
  else
    Serial.println(F("\nTemperatura enviado!"));

  Serial.println(valor);
}

void Lampada_publish(void) {
  bool valor = detectorPresenca();

  if (valor) { //envia e testa ao mesmo tempo
    _LampadaP.publish("HIGH");
  }
  else {
    _LampadaP.publish("LOW");
  }

  Serial.println(valor);
}

void Controle_publish(void) {
  bool valor = detectorControle("OK");

  if (!valor) { //envia e testa ao mesmo tempo
    _ControleP.publish("Auto");
  }
  else {
    _ControleP.publish("Manual");
  }
}

void Ventilador_publish(void) {
  bool presenca = detectorPresenca();
  int temp = (int) temperatura(10, DELAY_);
  bool stt;

  if (presenca && temp >= TEMP_MAX) {
    stt = _VentiladorP.publish("HIGH");
  }
  else {
    stt = _VentiladorP.publish("LOW");
  }

  if (!stt)
    Serial.print(F("\nFalha ao enviar Ventilador"));
  else
    Serial.println(F("\n Ventilador enviado!"));

  delay(10);
}

void Lampada_callback(char *data, uint16_t len) {
  String printToSerial = data;
  static long int tempoAtual= 0;
  
  if (printToSerial.compareTo("HIGH")) {
    digitalWrite(PORTA_3, HIGH);
    if(tempoAtual == 0){
      tempoAtual= millis();
    }
    else{
      TempoTotal+= millis() - tempoAtual;
      tempoAtual= 0;
    }
  }
  else {
    digitalWrite(PORTA_3, LOW);
    TempoTotal+= millis() - tempoAtual;
    tempoAtual= 0;
  }

  Serial.print("Servidor: ");
  Serial.println(printToSerial);
}

void Ventilador_callback(char *data, uint16_t len) {
  String printToSerial = data;

  if (printToSerial.compareTo("HIGH")){
    digitalWrite(PORTA_4, HIGH);
  }
  else
    digitalWrite(PORTA_4, LOW);

  Serial.print("Servidor: ");
  Serial.println(printToSerial);
}

void Controle_callback(char *data, uint16_t len){
  detectorControle(data);
}

void conectar_broker() {
  int8_t ret;

  if (mqtt.connected()) {
    return;
  }
  Serial.println("Conectando - se ao CloudMQTT...");
  uint8_t num_tentativas = 3;
  while ((ret = mqtt.connect()) != 0) {
    Serial.println(mqtt.connectErrorString(ret));
    Serial.println("Falha. Tentando se reconectar em 1 segundos.");
    mqtt.disconnect();
    delay(1000);
    num_tentativas--;
  }
}
