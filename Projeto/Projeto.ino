#include "Projeto.h"

Ultrasonic ultrasonic(14, 27);

extern WiFiClient client;
extern Adafruit_MQTT_Client mqtt;//(&client, "io.adafruit.com", 1883 , IO_USERNAME, IO_KEY);

bool vez= false;

void setup() {

  Serial.begin(115200);
  Serial.println("Inicialização OTA...");

  //define wifi como station (estação)
  WiFi.mode(WIFI_STA);

  //inicializa wifi
  WiFi.begin(WIFI_SSID, WIFI_PASS);

  beginOTA();

  /* Reles */
  pinMode(PORTA_4, OUTPUT);
  pinMode(PORTA_3, OUTPUT);
  digitalWrite(PORTA_4, HIGH);
  digitalWrite(PORTA_3, HIGH);
  //////////

  pinMode(PIN_TEMP, INPUT);
  initMQTT();
}

void loop() {
  conectar_broker();
  mqtt.processPackets(5000);
  ArduinoOTA.handle();

  if(vez){
    if (!detectorControle("OK")) {
      Ventilador_publish();
      Lampada_publish();
    }
  }
  else{
    Temperatura_publish();
    Controle_publish();
    TempoLampada_publish();
  }

  delay(1000);
  vez= !vez;
}

float temperatura(byte n_filtro, byte delay_) {
  long int valor = 0;

  for (int i = 0; i < n_filtro; i++) {
    valor += analogRead(PIN_TEMP);
    delay(delay_ / n_filtro);
  }

  valor /= n_filtro;

  return valor * 3.3 * 100 / 4096;
}

bool detectorPresenca() {
  static bool _status = false;
  static long int tempo = 0;
  int valor;

  if (!_status || millis() - tempo >= TEMPO_ESPERA) {
    valor = ultrasonic.read();
    if (valor >= 4 && valor <= 10) {
      _status = true;
      tempo = millis();
    }
    else
      _status = false;
  }
  return _status;
}

bool detectorControle(char *tipo) {
  String strTipo = tipo;
  static bool Status = false;
  
  if(strTipo.compareTo("OK")){
    return Status;
  }
  
  if (strTipo.compareTo("Auto")) {
    Status = false;
  }
  else if (strTipo.compareTo("Manual")) {
    Status = true;
  }

  return Status;
}
