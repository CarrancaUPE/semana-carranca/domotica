/*Em comum OTA e IOT*/
#include <WiFi.h>

/*IOT*/
#include <Adafruit_MQTT.h>
#include <Adafruit_MQTT_Client.h>
////////

/*OTA*/
#include <ArduinoOTA.h> //lib do ArduinoOTA 
#include <ESPmDNS.h> //lib necessária para comunicação network
#include <WiFiUdp.h> //lib necessária para comunicação
///////

#include <Ultrasonic.h>

#define WIFI_SSID   "silassiemens"
#define WIFI_PASS   "silas123456"

/*IOT*/
#define IO_USERNAME  "carranca"
#define IO_KEY       "9106a13fc64a4c7e918b3f46485ae997"
///////

/*Relés*/
#define PORTA_4 5
#define PORTA_3 4
////////

#define TEMPO_ESPERA 500
#define PIN_TEMP 33
#define DELAY_ 1000
#define TEMP_MAX 26 //Se o sensor medir maior que o TEMP_MAX o sistema de resfriamento é ativado

/*OTA*/
void endOTA();
void beginOTA();
void startOTA();
void errorOTA(ota_error_t error);
void progressOTA(unsigned int progress, unsigned int total);
//////////////

/*IOT*/
void initMQTT();
void conectar_broker();

void Lampada_callback(char *data, uint16_t len);
void Led_callback(char *data, uint16_t len);
void Ventilador_callback(char *data, uint16_t len);
void Controle_callback(char *data, uint16_t len);

void Lampada_publish(void);
void Ventilador_publish(void);
void Temperatura_publish(void);
void Controle_publish(void);
void TempoLampada_publish(void);
////

bool detectorPresenca();
float temperatura(byte n_filtro, byte delay_);
bool detectorControle(char *tipo);
